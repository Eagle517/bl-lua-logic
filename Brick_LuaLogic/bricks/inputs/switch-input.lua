
return function(gate, argv)
	if #argv == 1 then
		gate.ports[1]:setstate(toboolean(argv[1]))
		gate.ports[2]:setstate(toboolean(argv[1]))
	else
		gate.ports[1]:setstate(not gate.ports[1].state)
		gate.ports[2]:setstate(not gate.ports[2].state)
	end
	gate:cb("1\t" .. bool_to_int[gate.ports[1].state])
end
