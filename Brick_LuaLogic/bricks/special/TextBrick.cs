
datablock fxDTSBrickData(LogicGate_TextBrick_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Special";
	uiName = "Text Brick";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Text Brick";
	logicUIDesc = "Resets on rise, increments based on pulse length";
	
	logicInit   = lualogic_readfile($LuaLogic::Path @ "bricks/special/text-init.lua"  );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/special/text-update.lua");

	numLogicPorts = 1;

	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "Inc";
};
lualogic_registergatedefinition("LogicGate_TextBrick_Data");

function LogicGate_TextBrick_Data::LuaLogic_Callback(%data, %brick, %args){
	%printname = getField(%args, 0);
	
	%brick.setPrint(lualogic_getprint(%printname));
}
