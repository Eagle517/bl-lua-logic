
return function(gate)
	if gate.ports[1]:isrising() then
		gate.tickStarted = gate:gettick()
	elseif gate.ports[1]:isfalling() then
		local ticksOn = gate:gettick() - gate.tickStarted
		
		local printid = ((ticksOn-1) % #textbrick_idxToPrint)
		local printname = textbrick_idxToPrint[printid]
		
		gate:cb("1\t" .. printname)
	end
end
