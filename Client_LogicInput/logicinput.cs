
//Logic Input
//by Redo
//Uses a move map to capture keystrokes and send them to the server, for use with a keyboard or something

function LogicInput_Start(){
	if(!$LogicInput_Active){
		$LogicInput_Active = true;
		
		LogicInput_MoveMap.push();
	}
}

function LogicInput_Stop(){
	if($LogicInput_Active){
		$LogicInput_Active = false;
		
		LogicInput_MoveMap.pop();
	}
}

///////////////////////////////////////////////////////////

function LogicInput_SendKey(%key, %state){
	commandToServer('LInputKey', %key, %state);
	echo("send \"" @ %key @ "\", " @ %state);
}

///////////////////////////////////////////////////////////

function clientCmdLStartInput(){
	LogicInput_Start();
}

function clientCmdLStopInput(){
	LogicInput_Stop();
}

///////////////////////////////////////////////////////////

LogicInput_Stop();
exec("./logicinputmovemap.cs");
