GateDefinition = {
	ports = {},
	logic = function(gate) end,
	input = function(gate, argv) end
}

local function collapseLogicFunction(text)
	text = text:gsub("\\r", "\r")
	text = text:gsub("\\n", "\n")
	text = text:gsub("\\t", "\t")
	text = text:gsub("\\:", ";" )
	
	text = text:gsub("\\/", "\\")
	
	return text
end

function GateDefinition:new(objref, name, description, init, logic, input, ports)
	local o = {
		objref = objref,
		name = name,
		description = description,
		ports = ports or {}
	}
	
	init = collapseLogicFunction(init)
	logic = collapseLogicFunction(logic)
	input = collapseLogicFunction(input)
	
	local initfunc = loadstring(tostring(init))
	if initfunc~=nil then
		o.init = initfunc() or function()end
	else
		print("Error loading init func for ".. (name or ""))
	end
	
	local logicfunc = loadstring(tostring(logic))
	if logicfunc ~= nil then
		o.logic = logicfunc() or function()end
	else
		print("Error loading logic function for " .. (name or ""))
	end

	local inputfunc = loadstring(tostring(input))
	if inputfunc ~= nil then
		o.input = inputfunc() or function()end
	else
		print("Error loading input function for " .. (name or ""))
	end

	setmetatable(o, self)
	self.__index = self
	return o
end

function GateDefinition:constructgate(objref, position, rotation)
	local gate = Gate:new(objref, self)

	for i = 1, #self.ports do
		local port = self.ports[i]
		local type = port.type
		local pos = {port.position[1], port.position[2], port.position[3]}
		local dir = port.direction

		if dir < 4 then
			dir = (dir + rotation) % 4
		end

		local x = pos[1]

		if rotation == 1 then
			pos[1] = pos[2]
			pos[2] = -x
		elseif rotation == 2 then
			pos[1] = -pos[1]
			pos[2] = -pos[2]
		elseif rotation == 3 then
			pos[1] = -pos[2]
			pos[2] = x
		end

		gate:addport(Port:new(type, dir, {position[1]+pos[1], position[2]+pos[2], position[3]+pos[3]}, port.causeupdate))
	end

	return gate
end
